package com.example.排序算法;

import java.util.Random;

/**
 * 分治排序
 * <p>
 * 1. 归并
 * 2. 快排
 *
 * @author 600847
 */
public class 分治排序 {
    public static void main(String[] args) {
        Random random = new Random();
        int[] sortArray = new int[50];
        for (int i = 0; i < 50; i++) {
            sortArray[i] = random.nextInt(100);
        }

        // 打印原数组
        for (int i : sortArray) {
            System.out.print(i + " ");
        }
        System.out.println(" ");

        // 归并排序
        // mergeSort(sortArray, 0, sortArray.length - 1);

        // 快速排序
        quickSort(sortArray, 0, sortArray.length - 1);

        // 快速排序 -- 哨兵
        quickSortWithSentry(sortArray, 0, sortArray.length - 1);

        // 打印结果
        for (int i : sortArray) {
            System.out.print(i + " ");
        }
    }

    /*********************  归并排序 begin  ***************/
    /**
     * 归并排序
     *
     * @param sortArray
     */
    private static void mergeSort(int[] sortArray, int start, int end) {
        if (start >= end) {
            return;
        }

        int mid = (start + end) / 2;

        mergeSort(sortArray, start, mid);
        mergeSort(sortArray, mid + 1, end);

        mergeTwoArray(sortArray, start, mid, end);
    }

    /**
     * 合并分区结果
     *
     * @param sortArray
     * @param start
     * @param mid
     * @param end
     */
    private static void mergeTwoArray(int[] sortArray, int start, int mid, int end) {
        int[] temp = new int[end - start + 1];
        int i = start, j = mid + 1;
        int k = 0;
        while (i <= mid && j <= end) {
            if (sortArray[i] <= sortArray[j]) {
                temp[k++] = sortArray[i++];
            } else {
                temp[k++] = sortArray[j++];
            }
        }
        while (i <= mid) {
            temp[k++] = sortArray[i++];
        }

        while (j <= end) {
            temp[k++] = sortArray[j++];
        }

        for (int x = 0; x < temp.length; x++) {
            sortArray[x + start] = temp[x];
        }
    }
    /*********************  归并排序 end  ***************/








    /*********************  快速排序 begin  ***************/

    private static void quickSort(int[] targetArray, int head, int tail) {
        if (head >= tail) {
            return;
        }

        int key = targetArray[tail];
        int i = head;
        for (int j = head; j < tail; j++) {
            if (targetArray[j] < key) {
                int temp = targetArray[i];
                targetArray[i++] = targetArray[j];
                targetArray[j] = temp;
            }
        }
        int temp = targetArray[tail];
        targetArray[tail] = targetArray[i];
        targetArray[i] = temp;


        quickSort(targetArray, head, i - 1);
        quickSort(targetArray, i + 1, tail);
    }


    /**
     * 快排 -- 哨兵模式
     * @param targetArray
     * @param head
     * @param tail
     */
    private static void quickSortWithSentry(int[] targetArray, int head, int tail) {
        if (head < tail) {
            int position = getIndex(targetArray, head, tail);
            quickSortWithSentry(targetArray, head, position - 1);
            quickSortWithSentry(targetArray, position + 1, tail);
        }
    }

    private static int getIndex(int[] targetArray, int head, int tail) {
        int key = targetArray[tail];

        while (head < tail) {
            while (head < tail && targetArray[head] <= key) {
                head++;
            }
            targetArray[tail] = targetArray[head];
            while (head < tail && targetArray[tail] >= key) {
                tail--;
            }
            targetArray[head] = targetArray[tail];
        }
        targetArray[tail] = key;

        return tail;
    }

    /*********************  快速排序 end  ***************/
}
