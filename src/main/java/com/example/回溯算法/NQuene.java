package com.example.回溯算法;

import java.util.ArrayList;
import java.util.List;

public class NQuene {
    private static List<List<String>> result = new ArrayList<>();


    public static void main(String[] args) {
        solveNQueens(4);
    }


    public static List<List<String>> solveNQueens(int n) {
        int[][] choose = new int[n][n];

        for (int i = 0; i < n; i++) {
            backtrack(choose, n, 0, i, new ArrayList<>());
        }

        return result;
    }

    // choose 大于0 则表示可被其他皇后攻击
    // curRow -> 当前行  curLine -> 当前列
    private static void backtrack(int[][] choose, int n , int curRow, int curLine, List<String> possibleResult) {
        if (curRow == n) {
            // 能走到这边说明之前结果校验通过，增加进结果
            System.out.println("!!!!!!! add");
            printList(possibleResult);
            printChoose(choose, n);
            result.add(new ArrayList<>(possibleResult));
            return;
        }

        // 大于0 则表示可被其他皇后攻击 回溯
        if (choose[curRow][curLine] > 0) {
            return;
        }

        // 标记可攻击
        markChoose(choose, n, curRow, curLine, -1);
        System.out.println("===== choose " + curRow);
        printChoose(choose, n);
        // 可行结果行增加
        System.out.println("===== possibleResult " + curRow);
        possibleResult.add(buildRow(n, curLine));
        printList(possibleResult);

        // 当前行 逐列进行可行性校验，一旦可行进入下一行
        for (int eachLine = 0; eachLine < n; eachLine++) {
            backtrack(choose, n, curRow + 1, eachLine, possibleResult);
        }

        // 回溯可攻击
        markChoose(choose, n, curRow, curLine, 1);
        System.out.println("===== choose return " + curRow);
        printChoose(choose, n);
        // 可行结果行回溯
        System.out.println("===== possibleResult return " + curRow);
        possibleResult.remove(curRow);
        printList(possibleResult);
    }

    private static String buildRow(int n, int curLine) {
        StringBuffer row = new StringBuffer("");
        for (int i = 0; i < n; i++) {
            if (i == curLine) {
                row.append("Q");
            } else {
                row.append(".");
            }
        }
        return row.toString();
    }

    // flag -> 正负1  -1 标记  1 取消标记
    private static void markChoose(int[][] choose, int n , int curRow, int curLine, int flag) {
        // 根据当前位置 标记行可被攻击
        for (int i = 0; i < n; i++) {
            choose[curRow][i] += (-1) * flag;
        }
        // 根据当前位置 标记列可被攻击
        for (int i = 0; i < n; i++) {
            choose[i][curLine] += (-1) * flag;
        }
        // 根据当前位置 标记斜线可被攻击  逐行标记，根据当前行和当前皇后位置进行标记
        for (int i = 0; i < n; i++) {
            int oneLine = curLine + (i - curRow);
            int anotherLine = curLine - (i - curRow);
            if (0 <= oneLine && oneLine < n) {
                choose[i][oneLine] += (-1) * flag;
            }
            if (0 <= anotherLine && anotherLine < n) {
                choose[i][anotherLine] += (-1) * flag;
            }
        }
    }



    private static void printList(List<String> possibleResult) {
        possibleResult.forEach(System.out::println);
    }

    private static void printChoose(int[][] choose, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(choose[i][j]);
                System.out.print("  ");
            }
            System.out.println();
        }
    }
}
